# FY25 Summer Internship

This will be used to run the Create FY25 Summer Internships. We'll be welcoming 4 interns, 2 each on [Remote Development](https://handbook.gitlab.com/handbook/engineering/development/dev/create/remote-development/) and  [Editor Extensions](https://handbook.gitlab.com/handbook/engineering/development/dev/create/editor-extensions/).

## Guides

- [Engineering Mentor Role](./docs/engineering_mentor.md)
