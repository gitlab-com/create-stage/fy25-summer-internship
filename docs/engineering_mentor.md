# Engineering Mentor Role

The Engineering Mentor role is a critical component to ensuring the success of the
Internship Program at GitLab. Engineering Mentors will be paired with an individual
intern at the time of onboarding.

## Goals

As a Engineering Mentor, your goal is to provide interns with:

1. An inside look into the day-to-day activities of a GitLab team member.
1. Technical and non-technical guidance as they encounter problems or questions.
1. Connections to learning about other parts of the organization.

## Use Descretion

If a situation demands it, the Engineering Mentor and other stakeholders are encouraged
to deviate from anything in this document.

Please use discretion and flexibility to tailor an awesome experience for
interns.

## Responsibilities

The role of Engineering Mentor is part [Onboarding Buddy][onboard-buddy] and part 
Engineer Shadow Host (think of how Sid hosts a [CEO Shadow][ceo-shadow]). The
program lasts from 10-12 weeks.

At the start of the intern's onboarding, the Engineering Mentor should:

1. Reach out on Slack to the new intern, introducing themselves as their Engineering
Mentor during their time at GitLab. Example:  

   ```
   Hi {name}! Welcome to GitLab! :wave:

   I'll be your Engineering Mentor during your time here at GitLab. I'm excited to help
   answer any questions you might have and connect you across other parts of the
   organization. You'll also get to shadow me during some of my meetings and
   activities.
   
   We'll chat synchronously soon, but in the meantime, keep an eye out in your
   email for:

     - Calendar invite for a `Engineering Mentor / Intern Welcome` meeting.

   Looking forward to working with you! :smile:
   ```
1. Setup the `Engineering Mentor / Intern Welcome` meeting.
   - This should be done as close as possible to the intern's start date.
   - During this meeting, the Engineering Mentor and Intern should get to know
   eachother, and the Engineering Mentor should answer any questions from the intern.
   - **IMPORTANT:** During this meeting, the Engineering Mentor and Intern should set
   up a recurring weekly `Engineering Mentor / Intern 1:1` meeting.
   - This is a good opprtunity to talk about the intern's specific goals during
   their time at GitLab.
1. Add the intern as a guest to meetings on the Engineering Mentor's calendar.
   - Try to invite them to as many meetings as possible, even 1:1's (at your
   discretion of course). Think of how transparently Sid runs the CEO Shadow
   program.
   - **IMPORTANT:** During a meeting, if at any time the topic isn't appropriate
   for a third-party shadow, please kindly ask the intern to hop off the call
   and continue the meeting. After the meeting, send a message to the intern on
   Slack, thanking them for their flexibility and understanding.

Throughout the internship program, after the intern has started, the Engineering Mentor
should:

1. Host the intern as a shadow to the Engineering Mentor's regular meetings.
1. Check in with the intern during the weekly `Engineering Mentor / Intern 1:1`. One
possible agenda for this meeting:  
   
   ```
   - How did last weeks goals go?
   - What are the goals for this week?
   - What interactions have you had that stuck out to you this week?
   - Any ideas or questions you'd like to discuss?
   - Any highlights from the intern's daily reflections?
   ```  

1. Assist the intern with ad-hoc questions and issues.
   - Interns will be given priorities and tasks through a separate channel. You
   are not responsible for giving the intern work and priorities. Your task is
   to be the Point-of-Contact and guide for the intern.
   - **IMPORTANT:** You do not need to have all the answers. A lot can be
   learned by demonstarting how to get help for questions through the right
   channels.
1. Connect the intern with other parts of the company.
   - Try to periodically introduce the intern to a team member from another
   department and encourage them to set up coffee chats. This is a great time
   for the intern to learn about other parts of the company.
   - Aim for connecting the intern with 8 other GitLab team members by the end
   of the intership program.

[onboarding-buddy]: https://handbook.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/
[ceo-shadow]: https://handbook.gitlab.com/handbook/ceo/shadow/
